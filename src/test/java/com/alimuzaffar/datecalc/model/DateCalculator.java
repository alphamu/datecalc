package com.alimuzaffar.datecalc.model;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Helper class that uses Java API's to calculate the day difference
 * This class is used by the unit tests to determine whether out calculations
 * are correct.
 */
public class DateCalculator {

    public static BigDecimal DAY_IN_MILLISECONDS = new BigDecimal(86400000d);

    public static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");


    public static int getDaysDiff(Date startDate, Date endDate) {
        Calendar startCal = Calendar.getInstance();
        Calendar endCal = Calendar.getInstance();

        startCal.setTime(startDate);
        endCal.setTime(endDate);

        return getDaysDiff(startCal, endCal);
    }

    public static int getDaysDiff(Calendar startCal, Calendar endCal) {
        startCal.set(Calendar.HOUR_OF_DAY, 23);
        startCal.set(Calendar.MINUTE, 59);
        startCal.set(Calendar.SECOND, 59);
        startCal.set(Calendar.MILLISECOND, 999);

        endCal.set(Calendar.HOUR_OF_DAY, 0);
        endCal.set(Calendar.MINUTE, 0);
        endCal.set(Calendar.SECOND, 0);
        endCal.set(Calendar.MILLISECOND, 0);

        return new BigDecimal(endCal.getTimeInMillis() - startCal.getTimeInMillis()).divide(DAY_IN_MILLISECONDS, RoundingMode.HALF_UP).intValue();
    }

    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        do {
            try {
                System.out.println("\nUsage:\n" +
                        "Type 'q' to exit.\n" +
                        "type 't' in either field to run automated tests.\n" +
                        "Please enter dates in the format dd/MM/yyyy");
                System.out.print("Please enter start date [dd/MM/yyyy]: ");
                String startDateTxt = br.readLine();
                startDateTxt = startDateTxt.trim();
                if (startDateTxt.equals("q")) {
                    break;
                } else if (startDateTxt.equals("t")) {
                    runAutomatedTests();
                    continue;
                }

                System.out.print("Please enter end date [dd/MM/yyyy]: ");
                String endDateTxt = br.readLine();
                endDateTxt = endDateTxt.trim();
                if (endDateTxt.equals("q")) {
                    break;
                } else if (endDateTxt.equals("t")) {
                    runAutomatedTests();
                    continue;
                } else {
                    processDates(startDateTxt, endDateTxt);
                }
            } catch (IllegalArgumentException iae) {
                System.err.println(iae.getMessage() + "\n");
            } catch (Exception e) {
                System.err.println("Input was invalid.\n");
            }
        } while (true);
    }

    public static int processDates(String startDateTxt, String endDateTxt)
            throws IllegalArgumentException, ParseException {
        validateInput(startDateTxt);
        startDateTxt = startDateTxt.trim();

        validateInput(endDateTxt);
        endDateTxt = endDateTxt.trim();

        Date startDate = DATE_FORMAT.parse(startDateTxt);
        Date endDate = DATE_FORMAT.parse(endDateTxt);

        if (startDate.after(endDate)) {
            System.out.println(startDateTxt + " - " + endDateTxt +
                    " = Start date cannot be after end date.\nShowing result for ");
            Date temp = startDate;
            startDate = endDate;
            endDate = temp;
        }

        int daysDiff = getDaysDiff(startDate, endDate);
        System.out.println(startDateTxt + " - " + endDateTxt +
                " = " +
                daysDiff);
        return daysDiff;
    }

    private static void validateInput(String txt)
            throws IllegalArgumentException {
        if (txt == null || txt.length() < 8 || txt.length() > 10) {
            throw new IllegalArgumentException("Date should be in format dd/MM/yyyy");
        }
    }

    private static void runAutomatedTests()
            throws IllegalArgumentException, ParseException {
        processDates("02/06/1983", "22/06/1983");
        processDates("04/07/1983", "25/12/1983");
        //processDates("03/08/1983", "03/01/1989");
        processDates("03/01/1989", "03/08/1983");

    }

    /*
    02/06/1983 - 22/06/1983 = 19
    04/07/1983 - 25/12/1983 = 173
    03/01/1989 - 03/08/1983 = 1979
     */
}
