package com.alimuzaffar.datecalc.model;

import junit.framework.TestCase;
import org.junit.Test;

/**
 * Test class used to test whether our day diff calculations
 * is correct over long periods of time.
 */
public class ADateLongRangeTest extends TestCase {

    @Test
    public void testFull() throws Exception {
        ADate start = new ADate(1901, 1, 1);
        ADate end = new ADate(2100, 12, 31);
        int val = DateCalculator.processDates("01/01/1901", "31/12/2100");
        assertEquals(val, start.diffInDays(end));
        System.out.println("FULL RANGE - PASS");
    }

    @Test
    public void test5Years() throws Exception {
        ADate start = new ADate(1980, 3, 3);
        ADate end = new ADate(1985, 6, 2);
        int val = DateCalculator.processDates("03/03/1980", "02/06/1985");
        assertEquals(val, start.diffInDays(end));
        System.out.println("5 YEAR - PASS");
    }

    @Test
    public void testAfterLeapDayOverYear() throws Exception {
        ADate start = new ADate(1980, 3, 3);
        ADate end = new ADate(1990, 6, 2);
        int val = DateCalculator.processDates("03/03/1980", "02/06/1990");
        assertEquals(val, start.diffInDays(end));
        System.out.println("10 YEAR - PASS");
    }

}
