package com.alimuzaffar.datecalc.model;

import junit.framework.TestCase;
import org.junit.Test;

/**
 * Test class to determin wehter our day difference is correct
 * when the start date and end date are a calendar year apart.
 * This may be less than 365 days or more.
 */
public class ADateNextYearTest extends TestCase {

    @Test
    public void testLessThanAYear() throws Exception {
        ADate start = new ADate(2015, 6, 1);
        ADate end = new ADate(2016, 2, 22);
        int val = DateCalculator.processDates("01/06/2015", "22/02/2016");
        assertEquals(val, start.diffInDays(end));

        start = new ADate(2014, 12, 30);
        end = new ADate(2015, 1, 4);
        val = DateCalculator.processDates("30/12/2014", "04/01/2015");
        assertEquals(val, start.diffInDays(end));

        System.out.println("LESS THAN YEAR - PASS");
    }

    @Test
    public void testAfterLeapDay() throws Exception {
        ADate start = new ADate(2015, 6, 1);
        ADate end = new ADate(2016, 3, 22);
        int val = DateCalculator.processDates("01/06/2015", "22/03/2016");
        assertEquals(val, start.diffInDays(end));
        System.out.println("LESS THAN A YEAR LEAP DAY - PASS");
    }

    @Test
    public void testAfterLeapDayOverYear() throws Exception {
        ADate start = new ADate(2015, 6, 1);
        ADate end = new ADate(2016, 6, 22);
        int val = DateCalculator.processDates("01/06/2015", "22/06/2016");
        assertEquals(val, start.diffInDays(end));
        System.out.println("OVER A YEAR WITH LEAP DAY - PASS");
    }

    @Test
    public void testAfterNoLeapDayOverYear() throws Exception {
        ADate start = new ADate(2010, 6, 1);
        ADate end = new ADate(2011, 6, 22);
        int expected = DateCalculator.processDates("01/06/2010", "22/06/2011");
        int actual = start.diffInDays(end);
        assertEquals(expected, actual);
        System.out.println("OVER A YEAR WITH LEAP DAY - PASS");
    }
}
