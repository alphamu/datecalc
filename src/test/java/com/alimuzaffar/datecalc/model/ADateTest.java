package com.alimuzaffar.datecalc.model;

import junit.framework.TestCase;
import org.junit.Test;

/**
 * Unit test class to check if our AInt and ADate objects
 * set up properly.
 */
public class ADateTest extends TestCase {
    /**
     * Check whether out AInt object works as expected.
     */
    @Test
    public void testIntObj() {
        AInt num1 = new AInt("123");
        assertEquals(123, num1.intValue());

        AInt num2 = new AInt("-123");
        assertEquals(-123, num2.intValue());

        AInt num3 = new AInt("0");
        assertEquals(0, num3.intValue());

        AInt num4 = new AInt("1");
        assertEquals(1, num4.intValue());

        AInt num5 = new AInt("-1");
        assertEquals(-1, num5.intValue());
    }

    @Test
    public void testADate() {
        ADate notLeapYear = new ADate(2010, ADate.FEB, 1);
        assertEquals(28, notLeapYear.daysInMonth());
        assertEquals(365, notLeapYear.daysInYear()); //int
        assertEquals(365, ADate.daysInYear(2010)); //Calculated by adding days in each month

        ADate leapYear = new ADate(2012, ADate.FEB, 1);
        assertEquals(29, leapYear.daysInMonth());
        assertEquals(366, leapYear.daysInYear()); //int
        assertEquals(366, ADate.daysInYear(2012)); //Calculated by adding days in each month

    }

    @Test
    public void testRecommendedDates() {
        ADate start = new ADate(1983, 6, 2);
        ADate end = new ADate(1983, 6, 22);
        assertEquals(19, start.diffInDays(end));
        System.out.println("Diff = " + start.diffInDays(end) + " pass");

        start = new ADate(1983, 7, 4);
        end = new ADate(1983, 12, 25);
        assertEquals(173, start.diffInDays(end));
        System.out.println("Diff = " + start.diffInDays(end) + " pass");

        start = new ADate(1989, 1, 3);
        end = new ADate(1983, 8, 3);
        assertEquals(1979, start.diffInDays(end));
        System.out.println("Diff = " + start.diffInDays(end) + " pass");

    }


}
