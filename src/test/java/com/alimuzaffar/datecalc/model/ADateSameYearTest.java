package com.alimuzaffar.datecalc.model;

import junit.framework.TestCase;
import org.junit.Test;

/**
 * A test class that check whether date calculations are correct
 * for when the start date and end date are in the same year.
 */
public class ADateSameYearTest extends TestCase {

    @Test
    public void testSameMonth() throws Exception {
        ADate start = new ADate(2015, 6, 1);
        ADate end = new ADate(2015, 6, 22);
        int val = DateCalculator.processDates("01/06/2015", "22/06/2015");
        assertEquals(val, start.diffInDays(end));
        System.out.println("SAME MONTH - PASS");
    }

    @Test
    public void testBeforeLeapDay() throws Exception {
        //START BEFORE LEAP DAY
        ADate start2 = new ADate(2016, 2, 14);
        ADate end2 = new ADate(2016, 6, 22);
        int val2 = DateCalculator.processDates("14/02/2016", "22/06/2016");
        assertEquals(val2, start2.diffInDays(end2));
        System.out.println("START BEFORE LEAP DAY - PASS");
    }

    @Test
    public void testBeforeLeapDayNotLeapYear() throws Exception {
        //START BEFORE LEAP DAY (NOT LEAP YEAR)
        ADate start3 = new ADate(2015, 2, 14);
        ADate end3 = new ADate(2015, 6, 22);
        int val3 = DateCalculator.processDates("14/02/2015", "22/06/2015");
        assertEquals(val3, start3.diffInDays(end3));
        System.out.println("START BEFORE LEAP DAY (NOT LEAP YEAR) - PASS");
    }

    @Test
    public void testLeapMonth() throws Exception {
        //START BEFORE LEAP DAY (NOT LEAP YEAR)
        ADate start3 = new ADate(2016, 2, 1);
        ADate end3 = new ADate(2016, 3, 1);
        int val3 = DateCalculator.processDates("01/02/2016", "01/03/2016");
        assertEquals(val3, start3.diffInDays(end3));
        System.out.println("TEST LEAP MONTH - PASS");
    }
}
