package com.alimuzaffar.datecalc.model;

/**
 * ADate class that contains date calculations methods.
 */
public class ADate implements Comparable<ADate> {
    public static final int INVALID_MONTH = 0;
    public static final int JAN = 1;
    public static final int FEB = 2;
    public static final int MAR = 3;
    public static final int APR = 4;
    public static final int MAY = 5;
    public static final int JUN = 6;
    public static final int JUL = 7;
    public static final int AUG = 8;
    public static final int SEP = 9;
    public static final int OCT = 10;
    public static final int NOV = 11;
    public static final int DEC = 12;

    private static final int[] DAYS_IN_MONTHS = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public static final int DAYS_IN_YEAR = 365;
    public static final int DAYS_IN_LEAP_YEAR = 366;

    private static final int YEAR_MIN = 1901;

    private int[] mDaysInMonthsThisYear;

    private int mYear;
    private int mMonth;
    private int mDay;

    private int mDaysInYear = DAYS_IN_YEAR;

    public ADate(String year, String month, String day) {
        this(new AInt(year), new AInt(month), new AInt(day));
    }

    /**
     * Constructor to build an ADate object using AInt.
     *
     * @param year  int greater than 1901.
     * @param month int ADate.JAN - ADate.DEC (1 - 12)
     * @param day   int between 1-31 depending on the month.
     */
    public ADate(AInt year, AInt month, AInt day) {
        this(year.intValue(), month.intValue(), day.intValue());
    }

    /**
     * Constructor to build an ADate object using ints.
     *
     * @param year  int greater than 1901.
     * @param month int ADate.JAN - ADate.DEC (1 - 12)
     * @param day   int between 1-31 depending on the month.
     */
    public ADate(int year, int month, int day) {
        if (year < YEAR_MIN) {
            throw new IllegalArgumentException("Year should be greater than 1900.");
        } else if (month < 1 || month > 12) {
            throw new IllegalArgumentException("Month should be between 1 and 12 (inclusive).");
        } else if (day < 1) {
            throw new IllegalArgumentException("Day cannot be less than 1.");
        }
        mYear = year;
        mMonth = month;
        mDay = day;
        initDaysArray();
        if (isLeapYear(mYear)) {
            mDaysInMonthsThisYear[FEB] = 29; //Feb has 29 days.
            mDaysInYear = DAYS_IN_LEAP_YEAR; //Year has 366 days.
        }
        if (mDay > mDaysInMonthsThisYear[mMonth]) {
            throw new IllegalArgumentException(monthToString(mMonth) + " only has " + mDaysInMonthsThisYear[mMonth] + " days");
        }
    }

    ////////////// DAYS RELATED FUNCTIONS

    /**
     * The number of days that have passed since the start of the year.
     *
     * @return int number of days passed
     */
    public int daysSinceStartOfYear() {
        int days = 0;
        int indexMonth = JAN;
        while (indexMonth < mMonth) {
            days += mDaysInMonthsThisYear[indexMonth++];
        }
        days += mDay;
        return days;
    }

    /**
     * The number of days that are remaining in the year.
     *
     * @return int number of days left
     */
    public int daysToTheEndOfYear() {
        int indexMonth = mMonth;
        int days = (mDaysInMonthsThisYear[indexMonth++] - mDay);
        while (indexMonth <= DEC) {
            days += mDaysInMonthsThisYear[indexMonth++];
        }

        return days;
    }

    /**
     * Returns the difference in days between the current date
     * and the ADate object passed in. By default, the start date
     * and the end date are not included in the difference in days.
     *
     * @param otherDate ADate object to calculate the difference in days.
     * @return different in the two dates in days, not including the
     * start and end dates.
     */
    public int diffInDays(ADate otherDate) {
        return diffInDays(otherDate, false, false);
    }

    /**
     * Returns the difference in days between the current date
     * and the ADate object passed in.
     *
     * @param otherDate ADate object to calculate the difference in days.
     * @param includeStartDate boolean include start date in days difference calculations.
     * @param includeEndDate boolean include end date in days difference calculations.
     * @return different in the two dates in days
     */
    public int diffInDays(ADate otherDate, boolean includeStartDate, boolean includeEndDate) {
        if (otherDate == null) {
            throw new NullPointerException("otherDate cannot be null.");
        }
        final int comp = compareTo(otherDate);
        int days = 0;
        int yearDiff = otherDate.mYear - mYear;
        if (yearDiff < 0) {
            yearDiff *= -1;
        }
        ADate startDate;
        ADate endDate;
        if (comp == 0) {
            return days;
        } else if (comp < 0) {
            // This is start date, otherDate is end date
            startDate = this;
            endDate = otherDate;
        } else {
            // otherDate is the start date, this is the end date
            startDate = otherDate;
            endDate = this;
        }

        days = endDate.daysSinceStartOfYear() - startDate.daysSinceStartOfYear();
        for (int i = 0; i < yearDiff; i++) {
            days += daysInYear(startDate.mYear + i);
        }

        if (includeStartDate && includeEndDate) {
            days += 1;
        } else if (!includeStartDate && !includeEndDate) {
            days -= 1;
        }
        return days;
    }

    ////////////// MONTH RELATED FUNCTIONS
    /**
     * Returns the number of days in the current month
     * represented by this date object.
     *
     * @return int the number of days in the month.
     */
    public int daysInMonth() {
        return mDaysInMonthsThisYear[mMonth];
    }


    ////////////// YEAR RELATED FUNCTIONS
    /**
     * Years whether the date represented by this object is a leap year.
     *
     * @return is the current year a leap year.
     */
    public boolean isLeapYear() {
        return isLeapYear(mYear);
    }

    /**
     * The number of days in the year as represented by this object.
     *
     * @return number of days in the current year.
     */
    public int daysInYear() {
        return mDaysInYear;
    }

    /**
     * Returns whether or not the given year is a leap year.
     *
     * @param year the current year, must be greater than 1901
     * @return boolean whether leap year or not
     */
    public static boolean isLeapYear(int year) {
        if (year < YEAR_MIN) {
            throw new IllegalArgumentException("Year should be greater than 1900.");
        }
        if (year % 4 != 0) {
            return false;
        } else if (year % 100 != 0) {
            return true;
        } else if (year % 400 != 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Returns the number of days in the year.
     *
     * @param year year for which to calculate the number of years.
     * @return int number of days.
     */
    public static int daysInYear(int year) {
        if (year < YEAR_MIN) {
            throw new IllegalArgumentException("Year must be greater than 1900.");
        }
        // Faster approach
        return isLeapYear(year)? DAYS_IN_LEAP_YEAR : DAYS_IN_YEAR;

        // More extensible approach
        /*int days = 0;
        for (int d : DAYS_IN_MONTHS) {
            days += d;
        }

        if (isLeapYear(year)) {
            days += 1;
        }

        return days;
        */
    }

    /**
     * Takes the given month as represented by an integer and returns a
     * string representation of the month.
     *
     * @param month
     * @return JAN for 1, FEB for 2 and so on.
     */
    public static String monthToString(int month) {
        switch (month) {
            case JAN:
                return "JAN";
            case FEB:
                return "FEB";
            case MAR:
                return "MAR";
            case APR:
                return "APR";
            case MAY:
                return "MAY";
            case JUN:
                return "JUN";
            case JUL:
                return "JUL";
            case AUG:
                return "AUG";
            case SEP:
                return "SEP";
            case OCT:
                return "OCT";
            case NOV:
                return "NOV";
            case DEC:
                return "DEC";
            default:
                throw new IllegalArgumentException(month + " is an invalid month.");
        }
    }

    /**
     * Determine whether the provided date is the before, after or the same
     * as the date represented by the current object.
     *
     * @param o ADate
     * @return 0 if the two dates are the same, > 1 if the current date
     * is larger and < 1 if the current date is smaller.
     */
    @Override
    public int compareTo(ADate o) {
        if (o == null) {
            throw new NullPointerException("parameter cannot be null.");
        }
        //convert both the dates to a 20151231 format which makes
        //then subtract them to determine which is larger
        int d1 = (mYear * 10000);
        d1 += (mMonth * 100);
        d1 += mDay;

        int d2 = (o.mYear * 10000);
        d2 += (o.mMonth * 100);
        d2 += o.mDay;

        return d1 - d2;
    }

    /**
     * Copy the static final DAYS_IN_MONTHS array to a member instance.
     */
    private void initDaysArray() {
        mDaysInMonthsThisYear = new int[DAYS_IN_MONTHS.length];
        for (int i = 0; i < mDaysInMonthsThisYear.length; i++) {
            mDaysInMonthsThisYear[i] = DAYS_IN_MONTHS[i];
        }
    }

    public String toString() {
        return String.format("%02d/%02d/%04d", mDay, mMonth, mYear);
    }

}
