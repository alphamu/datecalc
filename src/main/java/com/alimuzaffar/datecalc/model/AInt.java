package com.alimuzaffar.datecalc.model;

/**
 * AInt class that converts a String to an Integer.
 */
public class AInt {
    int mNum = 0;
    char[] mChar;

    public AInt(String s) {
        if (s == null || s.length() == 0) {
            throw new IllegalArgumentException("parameter s must have a value, cannot be null or empty");
        } else if (s.charAt(0) == '-' && s.length() == 1) {
            throw new IllegalArgumentException("parameter s must have a value, cannot be null or empty");
        }

        mChar = s.toCharArray();
        int multiplier = 1;
        for (int i = 0; i < mChar.length; i++) {
            int index = mChar.length - 1 - i;
            char c = mChar[index];
            int n = charToInt(c);
            if (c == '-' && index != 0) {
                throw new NumberFormatException(s + " is not an integer.");
            } else if (c != '-') {
                mNum += multiplier * n;
            } else {
                mNum *= n;
            }
            multiplier *= 10;
        }
    }

    public int charToInt(char c) {
        switch (c) {
            case '0':
                return 0;
            case '1':
                return 1;
            case '2':
                return 2;
            case '3':
                return 3;
            case '4':
                return 4;
            case '5':
                return 5;
            case '6':
                return 6;
            case '7':
                return 7;
            case '8':
                return 8;
            case '9':
                return 9;
            case '-':
                return -1;
            default:
                throw new NumberFormatException(String.valueOf(mChar) + " is not an integer.");
        }
    }

    public int intValue() {
        return mNum;
    }

    public String toString() {
        return String.valueOf(mNum);
    }
}
