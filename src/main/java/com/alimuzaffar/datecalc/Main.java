package com.alimuzaffar.datecalc;

import com.alimuzaffar.datecalc.model.ADate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Main class.
 */
public class Main {

    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        do {
            try {
                System.out.println("\nUsage:\n" +
                        "Type 'q' to exit.\n" +
                        "Please enter dates in the format dd/MM/yyyy");
                System.out.print("Please enter start date [dd/MM/yyyy]: ");
                String startDateTxt = br.readLine();
                startDateTxt = startDateTxt.trim();
                if (startDateTxt.equals("q")) {
                    break;
                }

                System.out.print("Please enter end date [dd/MM/yyyy]: ");
                String endDateTxt = br.readLine();
                endDateTxt = endDateTxt.trim();
                if (endDateTxt.equals("q")) {
                    break;
                } else {
                    ADate startDate = textToDate(startDateTxt);
                    ADate endDate = textToDate(endDateTxt);
                    System.out.println(startDate+ " - " + endDate +
                            " = " + startDate.diffInDays(endDate));
                }
            } catch (IllegalArgumentException iae) {
                System.err.println(iae.getMessage() + "\n");
            } catch (Exception e) {
                System.err.println("Input was invalid.\n");
            }
        } while (true);
        try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ADate textToDate(String dateTxt) {
        String split[] = dateTxt.split("[/\\- ]");
        if (split.length == 1 && dateTxt.length() == 8) {
            String[] temp = new String[3];
            temp[0] = dateTxt.substring(0, 2);
            temp[1] = dateTxt.substring(2, 4);
            temp[2] = dateTxt.substring(4, 8);
            split = temp;
        } else if (split.length != 3) {
            throw new IllegalArgumentException("Date format should be dd/MM/yyyy.");
        }
        return new ADate(split[2], split[1], split[0]);
    }
}
